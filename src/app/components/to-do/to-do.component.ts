import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.scss']
})
export class ToDoComponent implements OnInit {
  constructor() {}

  items = [];

  newItem = '';

  addItem() {
    // tslint:disable-next-line: triple-equals
    if (this.newItem != '') {
      this.items.push(this.newItem);
      this.newItem = '';
    }
  }

  removeItem(i) {
    this.items.splice(i, 1);
  }

  ngOnInit() {}
}
